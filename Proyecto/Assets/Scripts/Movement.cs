using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;


public class Movement : MonoBehaviour
{
    float direccionHorizontal, direccionVertical;
    public float rapidez;
    float tiempo;
    public float tiempoDeCambio;
    Vector3 posicionJugadorTamanio, posicionJugador, resta;
    public Animator animator;
    private void Start()
    {

        float[] rapidezInicial = { -rapidez,rapidez};
        int random = Random.Range(0, 2);
        int direccionRandom = Random.Range(0, 2);
        switch (direccionRandom)
        {
            case 0:
                direccionHorizontal = rapidezInicial[random];
                break;
            case 1:
                direccionVertical = rapidezInicial[random];
                break;
        }
        posicionJugadorTamanio = Camera.main.WorldToViewportPoint(new Vector3(transform.position.x + transform.localScale.x/2f, transform.position.y + transform.localScale.y / 2f, 0));
        posicionJugador = Camera.main.WorldToViewportPoint(new Vector3(transform.position.x, transform.position.y, 0));
        resta = posicionJugadorTamanio - posicionJugador;
    }

    void Update()
    {
        if (Input.GetButtonDown("Horizontal"))
        {
            direccionVertical = 0;
            if (Input.GetAxis("Horizontal") >= 0)
                direccionHorizontal = rapidez;
            else
                direccionHorizontal = -rapidez;
        }
        else if (Input.GetButtonDown("Vertical"))
        {
            direccionHorizontal = 0;
            if (Input.GetAxis("Vertical") >= 0)
                direccionVertical = rapidez;
            else
                direccionVertical = -rapidez;
        }
        transform.Translate(new Vector3(direccionHorizontal, direccionVertical, 0) * Time.deltaTime);
        if (direccionHorizontal > 0)
        {
            animator.SetFloat("Left", 0f);
            animator.SetFloat("Rigth", 1f);
            animator.SetFloat("Down", 0f);
            animator.SetFloat("Up", 0f);
        }
        else if (direccionHorizontal < 0)
        {
            animator.SetFloat("Left", 1f);
            animator.SetFloat("Rigth", 0f);
            animator.SetFloat("Down", 0f);
            animator.SetFloat("Up", 0f);
        }
        else if (direccionVertical > 0)
        {
            animator.SetFloat("Down", 0f);
            animator.SetFloat("Up", 1f);
            animator.SetFloat("Left", 0f);
            animator.SetFloat("Rigth", 0f);

        }
        else if (direccionVertical < 0)
        {
            animator.SetFloat("Down", 1f);
            animator.SetFloat("Up", 0f);
            animator.SetFloat("Left", 0f);
            animator.SetFloat("Rigth", 0f);
        }


        //tiempo += Time.deltaTime;
        //if (tiempo >= tiempoDeCambio)
        //{
        //    transform.position += new Vector3(direccionHorizontal, direccionVertical, 0);
        //    tiempo = 0;
        //}

        posicionJugador = Camera.main.WorldToViewportPoint(new Vector3(transform.position.x , transform.position.y, 0));

        if (posicionJugador.x + resta.x >= 1 || posicionJugador.x - resta.x <= 0 || posicionJugador.y + resta.y >= 1f || posicionJugador.y - resta.y <= 0)
            SceneManager.LoadScene("Game Over");

    }
    private void OnTriggerEnter2D(Collider2D collision)
    {
        

        if (collision.gameObject.tag == "fruit")
        {
            GameManager.Instance.points++;
            Destroy(collision.gameObject);
            AudioManager.Intance.ManagerAudio(collision.transform.position, 1);
        }
        else if (collision.gameObject.tag == "rock")
        {
            GameManager.Instance.life--;
            Destroy(collision.gameObject);
            AudioManager.Intance.ManagerAudio(collision.transform.position, 0);

        }
    }
}

