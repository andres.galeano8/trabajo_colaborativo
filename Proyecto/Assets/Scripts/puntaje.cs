using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class puntaje: MonoBehaviour
{
    public TextMeshProUGUI puntuacion;
    float cantidadDePuntos;
    float suma;
    float resta;

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.transform.tag == "PuntajePositivo")
        {
            float.TryParse(puntuacion.text, out suma);
            suma += cantidadDePuntos;
            puntuacion.text = suma.ToString();
        }
        else
        {
            float.TryParse(puntuacion.text, out resta);
            resta -= cantidadDePuntos;
            puntuacion.text = resta.ToString();
        }
    }

    //el script se integra al personaje y a las frutas y/o objetos para que sume la puntuacion
}
