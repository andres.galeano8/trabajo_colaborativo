using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour
{
    public static GameManager Instance;
    public GameObject fruit;
    public GameObject granada;
    public Transform player;
    public TMP_Text pointsInScreen;
    public GameObject[] lifesGO;
    public int life = 3;
    public int points;
    public float radio;
    float timeWait,timeUpdate, timeUpdateGranada, timeWaitGranada;

    private void Awake()
    {
        if (Instance==null)
        {
            Instance = this;
        }
    }
    private void Start()
    {
        timeWait = 2;
        timeUpdate = 0;
        timeUpdateGranada = 0;
        timeWaitGranada = 3;
        for (int i = 0; i < lifesGO.Length; i++)
        {
            lifesGO[i].SetActive(true);
        }
    }
    private void Update()
    {
        FruitSpawn();
        ScreenUpdates();
        timeUpdate += Time.deltaTime;
        timeUpdateGranada += Time.deltaTime;
        CurrentLife();
    }
    public void FruitSpawn()
    {
        Vector3 randomSpawn = new Vector3(Random.Range(-7.5f, 7.5f), Random.Range(-5f, 5f), 0f);
        while (Vector3.Distance(player.position,randomSpawn)<radio)
        {
            randomSpawn = new Vector3(Random.Range(-7.5f, 7.5f), Random.Range(-5f, 5f), 0f);
        }
        if (timeUpdate>timeWait)
        {
            GameObject go =  Instantiate(fruit, randomSpawn, Quaternion.identity);
            timeUpdate = 0;
            timeWait += 0.06f;
        }
        if (timeUpdateGranada > timeWaitGranada)
        {
            GameObject go = Instantiate(granada, randomSpawn, Quaternion.identity);
            timeUpdateGranada = 0;
            timeWaitGranada -= 0.02f;
        }
        if (timeWaitGranada < 0.5f)
        {
            timeWaitGranada = 0.5f;
        }

    }
    public void ScreenUpdates ()
    {
        string screenpoints = "POINTS: "+points.ToString("000");
        pointsInScreen.text = screenpoints;
    }
    private void OnDrawGizmos()
    {
        Gizmos.color = Color.red;
        Gizmos.DrawWireSphere(player.position, radio);  
    }
    public void CurrentLife()
    {
        for (int i = 0; i < lifesGO.Length; i++)
        {
          
                lifesGO[i].SetActive(i<life);
            
        }
        if (life <= 0) SceneManager.LoadScene("Game Over");
    }

}
