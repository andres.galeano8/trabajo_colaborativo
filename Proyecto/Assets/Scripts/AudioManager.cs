using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AudioManager : MonoBehaviour
{
    public AudioClip[] audios;
    public static AudioManager Intance;
    public AudioSource audioSorce;

    private void Awake()
    {
        if (Intance==null)
        {
            Intance = this;
        }
    }
    public void ManagerAudio(Vector3 posicion, int clipIndex)
    {
        transform.position = posicion;
        audioSorce.PlayOneShot(audios[clipIndex]);
    }
}
