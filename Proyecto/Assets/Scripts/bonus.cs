using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class bonus : MonoBehaviour
{
    Vector3 objetosspawn;
    float tiempodesalida;
    public Transform ritghlimit, leftlimit, uplimit, downlimit;
    public float tiempodeaparicion;
    public GameObject[] objetos;
    public GameObject snake;
    public int enemigos;
    // Start is called before the first frame update
    void Start()
    {
        objetosspawn = transform.position;
    }

    // Update is called once per frame
    void Update()
    {
        if (tiempodesalida >= tiempodeaparicion)
        {
            foreach (GameObject o in objetos)
            {
                tiempodesalida = 0;
                if (o.activeInHierarchy == false)
                {
                    objetosspawn.x = Random.Range(ritghlimit.position.x, leftlimit.position.x);
                    o.transform.position = objetosspawn;
                    o.SetActive(true);
                    break;
                }
            }
            foreach (GameObject o in objetos)
            {
                tiempodesalida = 0;
                if(o.activeInHierarchy == false)
                {
                    objetosspawn.y = Random.Range(uplimit.position.y, downlimit.position.y);
                    o.transform.position = objetosspawn;
                    o.SetActive(true);
                    break;
                }
            }

        }
        tiempodesalida += Time.deltaTime;
  
        if(tiempodesalida >= tiempodeaparicion)
        {
          
        }
    
     
    }
       
        
}
